FROM rust:1-slim as cargo-build

WORKDIR /app

COPY Cargo.toml Cargo.toml

RUN mkdir src
RUN echo "" > src/lib.rs
RUN echo "fn main() {println!(\"if you see this, the build broke\")}" > src/main.rs

RUN cargo build --release

COPY . .

RUN cargo build --release

FROM slim

WORKDIR /bin

COPY --from=cargo-build /app/target/release/users_api .

CMD ["users_api"]