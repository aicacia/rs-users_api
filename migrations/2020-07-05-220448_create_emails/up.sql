CREATE TABLE emails (
  id SERIAL PRIMARY KEY,
  user_id UUID NOT NULL REFERENCES users(id) ON DELETE RESTRICT,
  email TEXT NOT NULL,
  "primary" BOOLEAN NOT NULL DEFAULT 'f',
  confirmed BOOLEAN NOT NULL DEFAULT 'f',
  confirmation_token TEXT,
  inserted_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
  UNIQUE(email)
);