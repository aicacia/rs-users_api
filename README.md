# aicacia users api

aicacia users api

## Sign up
```bash
curl -i --header "Content-Type: application/json" \
  --request POST \
  --data '{"email":"example@mail.com","password":"password1234"}' \
  http://localhost:8080/users/sign_up/password
```

## Sign in
```bash
curl -i --header "Content-Type: application/json" \
  --request POST \
  --data '{"email":"example@mail.com","password":"password1234"}' \
  http://localhost:8080/users/sign_in/password
```

## Get user
```bash
curl -i --header "Content-Type: application/json" \
  --header "authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1OTcwMDE0NjYsInN1YiI6IjhiNzA3MzU4LWQxNzMtNGYyMC05ZDI3LTdkMjQwMzY1YzA2OCJ9.hBXetnBSIkGQZqwV4lep1uqlMjEeQnpTKYjGIGo2zoc" \
  http://localhost:8080/users/current
```