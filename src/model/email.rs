use diesel::{Associations, Identifiable, Queryable};
use serde::{Deserialize, Serialize};
use chrono::NaiveDateTime;

use crate::schema::emails;
use super::User;

#[derive(Identifiable, Associations, Serialize, Deserialize, Queryable)]
#[belongs_to(User)]
#[table_name = "emails"]
pub struct Email {
  pub id: i32,
  pub user_id: uuid::Uuid,
  pub email: String,
  pub primary: bool,
  pub confirmed: bool,
  pub confirmation_token: Option<String>,
  pub inserted_at: NaiveDateTime,
  pub updated_at: NaiveDateTime,
}
