use diesel::{Associations, Identifiable, Queryable};
use serde::{Deserialize, Serialize};
use chrono::NaiveDateTime;

use crate::schema::passwords;
use super::User;

#[derive(Identifiable, Associations, Serialize, Deserialize, Queryable)]
#[belongs_to(User)]
#[table_name = "passwords"]
pub struct Password {
  pub id: i32,
  pub user_id: uuid::Uuid,
  pub encrypted_password: String,
  pub inserted_at: NaiveDateTime,
  pub updated_at: NaiveDateTime,
}
