use chrono::NaiveDateTime;
use diesel::{Identifiable, Queryable};
use serde::{Deserialize, Serialize};

use crate::{model::Email, schema::users};

#[derive(Identifiable, Serialize, Deserialize, Queryable)]
#[table_name = "users"]
pub struct User {
  pub id: uuid::Uuid,
  pub inserted_at: NaiveDateTime,
  pub updated_at: NaiveDateTime,
}

#[derive(Serialize, Deserialize)]
pub struct PrivateUser {
  pub id: uuid::Uuid,
  pub inserted_at: NaiveDateTime,
  pub updated_at: NaiveDateTime,
  pub email: Option<Email>,
  pub emails: Vec<Email>,
}

impl PrivateUser {
  pub fn new(user: User, mut emails: Vec<Email>) -> Self {
    let email = emails
      .iter()
      .position(|email| email.primary && email.confirmed)
      .map(|index| emails.remove(index));
    Self {
      id: user.id,
      inserted_at: user.inserted_at,
      updated_at: user.updated_at,
      email,
      emails,
    }
  }
}