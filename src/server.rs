use std::net::ToSocketAddrs;

use actix_cors::Cors;
use actix_web::{http, web, App, HttpServer};

use crate::{
  controller,
  middleware::{self, auth::JwtSecret},
  repo,
};

pub async fn run<A: ToSocketAddrs>(url: A, jwt_secret: String, pool: repo::Pool) {
  HttpServer::new(move || {
    App::new()
      .wrap(
        Cors::new()
          .send_wildcard()
          .allowed_methods(vec!["GET", "POST", "PUT", "DELETE"])
          .allowed_headers(vec![http::header::AUTHORIZATION, http::header::ACCEPT])
          .allowed_header(http::header::CONTENT_TYPE)
          .max_age(3600)
          .finish(),
      )
      .wrap(actix_web::middleware::Compress::default())
      .wrap(actix_web::middleware::NormalizePath)
      .wrap(actix_web::middleware::Logger::default())
      .data(pool.clone())
      .data(JwtSecret(jwt_secret.clone()))
      .service(
        web::scope("/users")
          .service(
            web::scope("/sign_up").service(
              web::resource("/password")
                .route(web::post().to(controller::user::sign_up::password::handle)),
            ),
          )
          .service(
            web::scope("/sign_in").service(
              web::resource("/password")
                .route(web::post().to(controller::user::sign_in::password::handle)),
            ),
          )
          .service(
            web::resource("/current")
              .route(web::get().to(controller::user::current::handle))
              .wrap(middleware::auth::Auth::default()),
          ),
      )
  })
  .bind(url)
  .expect("Failed to start server.")
  .run()
  .await
  .expect("Unexpected error.");
}
