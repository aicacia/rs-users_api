use std::task::{Context, Poll};

use actix_service::{Service, Transform};
use actix_web::{
  dev::{ServiceRequest, ServiceResponse},
  http, Error,
};
use futures_util::future::{ok, Ready, LocalBoxFuture, FutureExt};
use headers::{authorization::Bearer, Authorization, Header};
use serde::{Deserialize, Serialize};

#[derive(Default, Clone, Copy)]
pub struct Auth;

impl<S, B> Transform<S> for Auth
where
  S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
  S::Future: 'static,
  B: 'static,
{
  type Request = ServiceRequest;
  type Response = ServiceResponse<B>;
  type Error = Error;
  type InitError = ();
  type Transform = AuthService<S>;
  type Future = Ready<Result<Self::Transform, Self::InitError>>;

  fn new_transform(&self, service: S) -> Self::Future {
    ok(AuthService { service })
  }
}

pub struct AuthService<S> {
  service: S,
}

impl<S, B> Service for AuthService<S>
where
  S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
  S::Future: 'static,
  B: 'static,
{
  type Request = ServiceRequest;
  type Response = ServiceResponse<B>;
  type Error = Error;
  type Future = LocalBoxFuture<'static, Result<Self::Response, Error>>;

  fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
    self.service.poll_ready(cx)
  }

  fn call(&mut self, mut req: ServiceRequest) -> Self::Future {
    let header = req
      .head()
      .headers()
      .get(http::header::AUTHORIZATION)
      .expect("failed to get Authorization Header");

    let authorization = Authorization::<Bearer>::decode(&mut vec![header.clone()].iter())
      .expect("failed to get Authorization Token");

    let token = authorization.0.token().to_owned();
    log::info!("Token: {:?}", token);

    let jwt_secret = req
      .app_data::<JwtSecret>()
      .expect("failed to get JwtSecret from app data.");
    let token_data =
      decode_token(jwt_secret.as_ref(), &token).expect("failed to decode Authorization Token");
    log::info!("Token Data: {:?}", token_data);

    req.head_mut().extensions_mut().insert(token_data.claims);

    let fut = self.service.call(req);

    async move {
      let mut res = fut.await?;

      res.headers_mut().insert(
        http::header::AUTHORIZATION,
        http::HeaderValue::from_str(&format!("Bearer {}", token)).expect("failed to create HeaderValue"),
      );
      
      Ok(res)
    }.boxed_local()
  }
}

pub struct JwtSecret(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct Claims {
  pub exp: i64,
  pub sub: uuid::Uuid,
}

pub fn encode_token(
  secret: &JwtSecret,
  user_id: uuid::Uuid,
) -> jsonwebtoken::errors::Result<String> {
  jsonwebtoken::encode(
    &jsonwebtoken::Header::default(),
    &Claims {
      exp: (chrono::Utc::now() + chrono::Duration::days(30)).timestamp(),
      sub: user_id,
    },
    &jsonwebtoken::EncodingKey::from_secret(secret.0.as_ref()),
  )
}

pub fn decode_token(
  secret: &JwtSecret,
  token: &str,
) -> jsonwebtoken::errors::Result<jsonwebtoken::TokenData<Claims>> {
  jsonwebtoken::decode::<Claims>(
    token,
    &jsonwebtoken::DecodingKey::from_secret(secret.0.as_ref()),
    &jsonwebtoken::Validation::default(),
  )
}
