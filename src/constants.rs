#[cfg(not(debug_assertions))] 
pub static HASH_COST: u32 = bcrypt::DEFAULT_COST;

#[cfg(debug_assertions)] 
pub static HASH_COST: u32 = 4;