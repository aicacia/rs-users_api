table! {
    emails (id) {
        id -> Int4,
        user_id -> Uuid,
        email -> Text,
        primary -> Bool,
        confirmed -> Bool,
        confirmation_token -> Nullable<Text>,
        inserted_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    passwords (id) {
        id -> Int4,
        user_id -> Uuid,
        encrypted_password -> Text,
        inserted_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    users (id) {
        id -> Uuid,
        inserted_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

joinable!(emails -> users (user_id));
joinable!(passwords -> users (user_id));

allow_tables_to_appear_in_same_query!(
    emails,
    passwords,
    users,
);
