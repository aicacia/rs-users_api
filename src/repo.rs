use diesel::{
  pg::PgConnection,
  r2d2::{self, ConnectionManager},
};

embed_migrations!();

pub type Connection = PgConnection;
pub type Pool = r2d2::Pool<ConnectionManager<Connection>>;

pub fn run<S: Into<String>>(url: S) -> Pool {
  let manager = ConnectionManager::<Connection>::new(url);
  let pool = r2d2::Pool::builder()
    .build(manager)
    .expect("Failed to create pool.");
  embedded_migrations::run(&pool.get().expect("Failed to get Connection from pool."))
    .expect("Failed to migrate.");
  pool
}
