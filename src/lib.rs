#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;

pub mod controller;
pub mod middleware;
pub mod model;
pub mod service;
pub mod constants;
pub mod repo;
pub mod schema;
pub mod server;