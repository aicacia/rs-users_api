use actix_web::{web, HttpRequest, HttpResponse, Responder};

use crate::{middleware::auth::Claims, repo::Pool, service::user};

pub async fn handle(request: HttpRequest, pool: web::Data<Pool>) -> impl Responder {
  let extensions = request.head().extensions();
  let claims = extensions
    .get::<Claims>()
    .expect("Failed to get claims from request");
    
  match pool.get() {
    Ok(connection) => match user::get::handle(&connection, claims.sub) {
      Ok(user) => HttpResponse::Ok().json(user),
      Err(err) => err.into(),
    },
    Err(err) => HttpResponse::InternalServerError().json(err.to_string()),
  }
}
