use actix_web::{http, web, HttpResponse, Responder};
use serde::Deserialize;

use crate::{
  middleware::auth::{encode_token, JwtSecret},
  repo::Pool,
  service::user,
};

#[derive(Deserialize)]
pub struct EmailPassword {
  email: String,
  password: String,
}

pub async fn handle(
  email_password: web::Json<EmailPassword>,
  jwt_secret: web::Data<JwtSecret>,
  pool: web::Data<Pool>,
) -> impl Responder {
  match pool.get() {
    Ok(connection) => {
      match user::sign_in::password::handle(
        &connection,
        &email_password.email,
        &email_password.password,
      ) {
        Ok(user) => HttpResponse::Ok()
          .header(
            http::header::AUTHORIZATION,
            format!(
              "Bearer {}",
              encode_token(jwt_secret.as_ref(), user.id).expect("failed to encode user_id")
            ),
          )
          .json(user),
        Err(err) => err.into(),
      }
    }
    Err(err) => HttpResponse::InternalServerError().json(err.to_string()),
  }
}
