use diesel::prelude::*;

use crate::{
  model::{Email, User, PrivateUser},
  repo::Connection,
  schema::users,
  service::error::Result,
};

pub fn handle(
  connection: &Connection,
  user_id: uuid::Uuid,
) -> Result<PrivateUser> {
  let user = users::dsl::users
    .find(user_id)
    .get_result::<User>(connection)?;
  let user_emails = Email::belonging_to(&user).load::<Email>(connection)?;
  Ok(PrivateUser::new(user, user_emails))
}
