use diesel::{insert_into, prelude::*};

use crate::{
  constants::HASH_COST,
  model::{Email, Password, PrivateUser, User},
  repo::Connection,
  schema::{emails, passwords, users},
  service::error::Result,
};

#[derive(Insertable)]
#[table_name = "emails"]
struct NewEmail<'a> {
  pub user_id: uuid::Uuid,
  pub email: &'a str,
}

#[derive(Insertable)]
#[table_name = "passwords"]
struct NewPassword {
  pub user_id: uuid::Uuid,
  pub encrypted_password: String,
}

pub fn handle(connection: &Connection, email: &str, password: &str) -> Result<PrivateUser> {
  let encrypted_password = bcrypt::hash(password, HASH_COST)?;

  connection.build_transaction().run(move || {
    let user = insert_into(users::table)
      .default_values()
      .get_result::<User>(connection)?;
    let email = insert_into(emails::table)
      .values(&NewEmail {
        user_id: user.id,
        email,
      })
      .get_result::<Email>(connection)?;
    let _password = insert_into(passwords::table)
      .values(&NewPassword {
        user_id: user.id,
        encrypted_password,
      })
      .get_result::<Password>(connection)?;

    Ok(PrivateUser::new(user, vec![email]))
  })
}
