use diesel::prelude::*;

use crate::{
  model::{Email, Password, PrivateUser},
  repo::Connection,
  schema::{emails, passwords},
  service::{self, error::Result},
};

pub fn handle(connection: &Connection, email: &str, password: &str) -> Result<PrivateUser> {
  let user_email = emails::dsl::emails
    .filter(emails::dsl::email.eq(email))
    //.filter(emails::dsl::confirmed.eq(true))
    .first::<Email>(connection)?;
  let user_id = user_email.user_id;

  let user_password = passwords::dsl::passwords
    .filter(passwords::dsl::user_id.eq(user_id))
    .first::<Password>(connection)?;

  let verified = bcrypt::verify(password, &user_password.encrypted_password)?;

  if verified {
    let private_user = service::user::get::handle(connection, user_id)?;
    Ok(private_user)
  } else {
    Err(diesel::result::Error::NotFound.into())
  }
}
