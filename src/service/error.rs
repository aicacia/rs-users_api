use std::fmt;

use actix_http::ResponseBuilder;
use actix_web::{error::ResponseError, http::StatusCode, HttpResponse};
use serde::{Deserialize, Serialize};

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug, Serialize, Deserialize)]
pub struct JsonError {
  message: String,
  status_code: u16,
}

impl<'a> From<&'a Error> for JsonError {
  fn from(error: &'a Error) -> Self {
    Self {
      message: error.to_string(),
      status_code: error.status_code().as_u16(),
    }
  }
}

#[derive(Debug)]
pub enum Error {
  Query(diesel::result::Error),
  Bcrypt(bcrypt::BcryptError),
}

impl fmt::Display for Error {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    match self {
      Self::Query(err) => err.fmt(f),
      Self::Bcrypt(err) => err.fmt(f),
    }
  }
}

impl std::error::Error for Error {}

impl From<diesel::result::Error> for Error {
  fn from(value: diesel::result::Error) -> Self {
    Error::Query(value)
  }
}

impl From<bcrypt::BcryptError> for Error {
  fn from(value: bcrypt::BcryptError) -> Self {
    Error::Bcrypt(value)
  }
}

impl ResponseError for Error {
  fn error_response(&self) -> HttpResponse {
    ResponseBuilder::new(self.status_code()).json(JsonError::from(self))
  }

  fn status_code(&self) -> StatusCode {
    match self {
      _ => StatusCode::INTERNAL_SERVER_ERROR,
    }
  }
}

impl Into<HttpResponse> for Error {
  fn into(self) -> HttpResponse {
    HttpResponse::from_error(self.into())
  }
}