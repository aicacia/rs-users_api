use std::env;

use dotenv::dotenv;
use users_api::{repo, server};

#[actix_rt::main]
async fn main() {
    dotenv().expect("Failed to get .env file.");
    env_logger::init();

    let database_url = env::var("DATABASE_URL").expect("Failed to get ENV var DATABASE_URL.");
    let pool = repo::run(&database_url);
    log::info!(
        "Connection Pool started {}.",
        database_url
    );

    let app_url = env::var("APP_URL").expect("Failed to get ENV var APP_URL.");
    log::info!(
        "Server started {}",
        app_url
    );

    let jwt_secret = env::var("JWT_SECRET").expect("Failed to get ENV var JWT_SECRET.");
    server::run(&app_url, jwt_secret, pool).await
}